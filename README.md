gemini-junit-reporter
======
Plugin for the [Gemini](https://gemini-testing.github.io) visual regression tester that enables loggin test results in JUnit's XML format.

## Installation

```bash
npm install --save-dev gemini-junit-reporter
```

## Usage

```yml
# In your Gemini configuration:
system:
  plugins:
    gemini-junit-reporter:
```

## Options

### `reportPath: string`

Default value: `junit.xml`

Path the JUnit XML report should be written to.

## Changelog

See [CHANGELOG](https://gitlab.com/Vinnl/gemini-junit-reporter/blob/master/CHANGELOG.md).

## License

MIT © [Vincent Tunru](https://vincenttunru.com)
