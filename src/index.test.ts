const mockWriteFile = jest.fn(() => (_path: string, _content: string, callback: Function) => callback());
jest.mock('fs', () => ({
  writeFile: mockWriteFile,
}));

const junitReporter = require('./index');

beforeEach(() => {
  mockWriteFile.mockClear();
});

function mockGemini() {
  type HandlerMap = {[eventName: string]: Function};
  const handlers = {
    geminiHandlers: {} as HandlerMap,
    runnerHandlers: {} as HandlerMap,
  };

  const gemini = {
    on: (eventName: string, handler: Function) => {
      handlers.geminiHandlers[eventName] = handler;

      if (eventName === 'startRunner') {
        handler({
          on: (runnerEventName: string, runnerHandler: Function) => handlers.runnerHandlers[runnerEventName] = runnerHandler,
        });
      }
    },
  };

  return [gemini, handlers] as [any, { geminiHandlers: HandlerMap, runnerHandlers: HandlerMap }];
}

it('should correctly serialise tests without results', () => {
  const [gemini, handlers] = mockGemini();

  junitReporter(gemini);

  expect(mockWriteFile.mock.calls.length).toBe(0);
  handlers.geminiHandlers.endRunner();
  expect(mockWriteFile.mock.calls.length).toBe(1);
  expect(mockWriteFile.mock.calls[0][1]).toMatchSnapshot();
});

it('should write test results to "junit.xml" if no path was configured', () => {
  const [gemini, handlers] = mockGemini();

  junitReporter(gemini);
  handlers.geminiHandlers.endRunner();

  expect(mockWriteFile.mock.calls[0][0]).toBe('junit.xml');
});

it('should write test results to the path passed as option', () => {
  const [gemini, handlers] = mockGemini();

  junitReporter(gemini, { reportPath: 'some-path' });
  handlers.geminiHandlers.endRunner();

  expect(mockWriteFile.mock.calls[0][0]).toBe('some-path');
});

it('should correctly serialise tests with different types of results', () => {
  const [gemini, handlers] = mockGemini();

  junitReporter(gemini, { reportPath: 'some-path' });
  // Add a successful result
  handlers.runnerHandlers.testResult({
    browserId: 'first-browser',
    equal: true,
    suite: { name: 'Successful test' },
  });
  // Add a failed result
  handlers.runnerHandlers.testResult({
    browserId: 'second-browser',
    equal: false,
    suite: { name: 'Failed test' },
  });
  // Add a skipped test
  handlers.runnerHandlers.skipState({
    browserId: 'arbitrary-browser',
    suite: { name: 'Skipped test' },
  });
  handlers.geminiHandlers.endRunner();
  // Add an error
  handlers.runnerHandlers.err({
    browserId: 'arbitrary-browser',
    suite: { name: 'Error' },
    message: 'Some error message',
  });
  handlers.geminiHandlers.endRunner();

  expect(makeIsomorphic(mockWriteFile.mock.calls[0][1])).toMatchSnapshot();
});

function makeIsomorphic(report: string) {
  return report.replace(/timestamp="(.+?)"/g, 'timestamp="1989-11-03T00:00:00.000Z"');
}
