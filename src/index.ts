import { promisify } from 'util';
import { writeFile } from 'fs';
import { TestSuiteReport, getJunitXml } from 'junit-xml';
import { Gemini, GeminiResult } from './geminiTypes';
import { TestCase } from 'junit-xml/TestResults';

export interface Options {
  reportPath?: string;
};

function junitReporter(gemini: Gemini, options: Options = {}) {
  const report: TestSuiteReport = {
    name: 'Gemini test result',
    suites: [],
  };

  gemini.on('startRunner', (runner: any) => {
    runner.on('testResult', (result: GeminiResult) => {
      const testCase: TestCase = {
        name: result.browserId,
      };
      if (result.equal === false) {
        testCase.failures = [ { message: 'Screenshot was not equal to the reference image' } ];
      }
      report.suites.push({
        name: result.suite.name,
        timestamp: new Date(),
        testCases: [ testCase ],
      });
    });

    runner.on('skipState', (result: GeminiResult) => {
      report.suites.push({
        name: result.suite.name,
        timestamp: new Date(),
        testCases: [
          {
            name: result.browserId,
            skipped: true,
          },
        ],
      });
    });

    runner.on('err', (error: Error & GeminiResult) => {
      report.suites.push({
        name: error.suite.name,
        timestamp: new Date(),
        testCases: [
          {
            name: error.browserId,
            errors: [ { message: error.message } ],
          },
        ],
      });
    });
  });

  gemini.on('endRunner', () => {
    const writeFilePromise = promisify(writeFile);

    return writeFilePromise(options.reportPath || 'junit.xml', getJunitXml(report));
  });
}

module.exports = junitReporter;
